class CookieManager {
  constructor(name, data) {
    this.name = name;
    this.data = data;
  }

  static setCookie(name, data) {
    let age = 900;
    return document.cookie = `${name}=${data}; max-age=${age}`;
  }

  static getCookie(name) {
    return ("; "+document.cookie).split(`; ${name}=`).pop().split(";").shift();
    
  }

  

  static clearCookie(name) {
   return document.cookie = `${name}=; max-age=0`;
   
  }
}

console.log(document.cookie);

//Examples to put in browser console.
//Set a Cookie
// CookieManager.setCookie("User","Jonah");

//Get a Cookie value from it's name.
// CookieManager.getCookie("User");

//Clear a Cookie by name.
// CookieManager.clearCookie("User");


//Old getCookie method.
// static getCookie(name) {
//   let matches = document.cookie.match(
//     new RegExp(
//       "(?:^|; )" +
//         name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") +
//         "=([^;]*)"
//     )
//   );
//   return matches ? decodeURIComponent(matches[1]) : undefined;
//     }