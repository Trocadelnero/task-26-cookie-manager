# Task 26 - Cookie Manager Class
---
## Write a JavaScript class called CookieManager with 3 methods.

- **setCookie(name, data);**
Set a cookie with the name as given argument and assign data given as argument. The cookie must expire in 15 minutes from the time it was set
- **getCookie(name);**
Return the value of a cookie based on the name given as an argument.
- **clearCookie(name);**
Delete a cookie based on the name given as an argument.

```
## Authors

Jonah DelNero
---
## Acknowledgments

Inspiration, code snippets, etc.
 **getCookie snippet**
* https://stackoverflow.com/questions/10730362/get-cookie-by-name/15724300#15724300
kirlich answered Mar 30 '13 at 22:40

* My Teacher Dewald Els for support.s

* https://javascript.info/cookie
* https://www.digitalocean.com/community/tutorials/understanding-classes-in-javascript
* https://www.w3schools.com/js/js_classes.asp
